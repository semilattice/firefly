{ pkgs ? import ./nix/nixpkgs.nix }:
rec {
  fireflyc = pkgs.haskellPackages.callPackage ./fireflyc {};
}
