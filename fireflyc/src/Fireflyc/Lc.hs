-- | Lambda calculus.
module Fireflyc.Lc
  ( Universe (..)
  , LcExpression (..)
  ) where

import GHC.TypeLits (type (+), Nat)

import Fireflyc.Name (Global, Local)

data Universe :: Nat -> * where
  Zero :: Universe 0
  Succ :: Universe n -> Universe (n + 1)
deriving stock instance Show (Universe n)

data LcExpression :: Nat -> * where
  LcVariable :: Local -> LcExpression n
  LcLambda :: [Local] -> LcExpression n -> LcExpression n
  LcCall :: Global -> [LcExpression n] -> LcExpression n
  LcApply :: LcExpression n -> [LcExpression n] -> LcExpression n
deriving stock instance Eq (LcExpression n)
deriving stock instance Show (LcExpression n)
