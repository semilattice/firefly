module Fireflyc.Intrinsic
  ( Intrinsic (..)
  , IntegerSize (..)
  , integerSizeBits
  ) where

data Intrinsic a

  = NotBool  a
  | AndBools a a
  | OrBools  a a
  | XorBools a a

  | AddIntegers IntegerSize a a

  | NotInteger  IntegerSize a
  | AndIntegers IntegerSize a a
  | OrIntegers  IntegerSize a a
  | XorIntegers IntegerSize a a

  | Panic a

  deriving stock (Eq, Functor, Foldable, Traversable, Show)

data IntegerSize
  = Byte | Short | Int
  deriving stock (Eq, Show)

integerSizeBits :: Integral a => IntegerSize -> a
integerSizeBits Byte  =  8
integerSizeBits Short = 16
integerSizeBits Int   = 32
