-- | Administrative normal-form abstract syntax tree data types.
module Fireflyc.Anf
  ( -- * Expressions
    AnfBlock (..)
  , AnfExpression (..)
  , AnfValue (..)

    -- * Variables
  , AnfLocal (..)

    -- * Analysis
  , anfBlockFree
  , anfExpressionFree
  , anfValueFree
  ) where

import Data.Hashable (Hashable)
import Data.HashSet (HashSet)
import Data.Semigroup ((<>))

import qualified Data.HashSet as HashSet

import Fireflyc.Intrinsic (Intrinsic (..))
import Fireflyc.Name (Global)

--------------------------------------------------------------------------------
-- Programs

-- | An ANF block is a sequence of let-bound expressions.
data AnfBlock =
  AnfBlock { anfBindings :: [(AnfLocal, AnfExpression)]
           , anfResult   :: AnfValue }
  deriving stock (Eq, Show)

-- | An ANF expression operates on values.
data AnfExpression
  = AnfValue AnfValue
  | AnfCall Global [AnfValue]
  | AnfApply AnfValue [AnfValue]
  | AnfIntrinsic (Intrinsic AnfValue)
  deriving stock (Eq, Show)

-- | An ANF value.
data AnfValue
  = AnfVariable AnfLocal
  | AnfLambda [AnfLocal] AnfBlock
  | AnfBool Bool
  deriving stock (Eq, Show)

--------------------------------------------------------------------------------
-- Variables

-- | An ANF local variable.
newtype AnfLocal =
  AnfLocal Word
  deriving stock (Eq, Show)
  deriving newtype (Hashable)

--------------------------------------------------------------------------------
-- Analysis

-- | Find the free variables of an ANF block.
anfBlockFree :: AnfBlock -> HashSet AnfLocal
anfBlockFree block =
  case anfBindings block of

    [] ->
      anfValueFree (anfResult block)

    (local, expression) : bindings ->
      anfExpressionFree expression <>
      let block' = block { anfBindings = bindings } in
        HashSet.delete local (anfBlockFree block')

-- | Find the free variables of an ANF expression.
anfExpressionFree :: AnfExpression -> HashSet AnfLocal

anfExpressionFree (AnfValue value) =
  anfValueFree value

anfExpressionFree (AnfCall _callee arguments) =
  foldMap anfValueFree arguments

anfExpressionFree (AnfApply applyee arguments) =
  foldMap anfValueFree (applyee : arguments)

anfExpressionFree (AnfIntrinsic intrinsic) =
  foldMap anfValueFree intrinsic

-- | Find the free variables of an ANF value.
anfValueFree :: AnfValue -> HashSet AnfLocal

anfValueFree (AnfVariable local) =
  HashSet.singleton local

anfValueFree (AnfLambda parameters body) =
  anfBlockFree body `HashSet.difference` parameters'
  where parameters' = HashSet.fromList parameters

anfValueFree (AnfBool _value) =
  HashSet.empty
