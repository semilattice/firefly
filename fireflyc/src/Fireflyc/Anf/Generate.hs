{-# LANGUAGE TemplateHaskell #-}

-- | Monad for generating ANF.
module Fireflyc.Anf.Generate
  ( -- * Monad
    MonadGenerate
  , State (..)
  , runGenerate
  , runGenerateT
  , rerunGenerateT
  , freshLocal

    -- * Expressions
  , expression
  , value
  , call
  , apply
  , intrinsic
  ) where

import Control.Lens ((<<%=), makeLenses)
import Control.Monad.State.Class (MonadState)
import Control.Monad.Trans.RWS (RWST, runRWST)
import Control.Monad.Trans.Writer (WriterT, runWriterT)
import Control.Monad.Writer.Class (MonadWriter)
import Data.DList (DList)
import Data.Functor.Identity (Identity, runIdentity)
import Data.Tuple (swap)

import qualified Control.Monad.Writer.Class as Writer
import qualified Data.DList as DList

import Fireflyc.Anf

import Fireflyc.Intrinsic (Intrinsic)
import Fireflyc.Name (Global)

--------------------------------------------------------------------------------
-- Monad

type MonadGenerate m = (MonadState State m, MonadWriter Bindings m)

data State =
  State { _stateNextLocal :: Word }
  deriving (Eq, Show)

type Bindings =
  DList (AnfLocal, AnfExpression)

$(makeLenses ''State)

runGenerate
  :: RWST () Bindings State Identity a
  -> ([(AnfLocal, AnfExpression)], a)
runGenerate = runIdentity . runGenerateT

runGenerateT
  :: Monad m
  => RWST () Bindings State m a
  -> m ([(AnfLocal, AnfExpression)], a)
runGenerateT action = do
  (a, _state, bindings) <- runRWST action () (State { _stateNextLocal = 0 })
  pure (DList.toList bindings, a)

-- | Run an ANF-generating action, using the same source of fresh locals as the
-- caller.
rerunGenerateT
  :: MonadState State m
  => WriterT Bindings m a
  -> m ([(AnfLocal, AnfExpression)], a)
rerunGenerateT = fmap swap . fmap (fmap DList.toList) . runWriterT

freshLocal :: MonadState State m => m AnfLocal
freshLocal = fmap AnfLocal $ stateNextLocal <<%= succ

--------------------------------------------------------------------------------
-- Expressions

expression :: MonadGenerate m => AnfExpression -> m AnfValue
expression rhs = do
  local <- freshLocal
  Writer.tell $ DList.singleton (local, rhs)
  pure (AnfVariable local)

value :: MonadGenerate m => AnfValue -> m AnfValue
value = expression . AnfValue

call :: MonadGenerate m => Global -> [AnfValue] -> m AnfValue
call = (expression .) . AnfCall

apply :: MonadGenerate m => AnfValue -> [AnfValue] -> m AnfValue
apply = (expression .) . AnfApply

intrinsic :: MonadGenerate m => Intrinsic AnfValue -> m AnfValue
intrinsic = expression . AnfIntrinsic
