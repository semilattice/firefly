{-# LANGUAGE TemplateHaskell #-}

-- | Translate LC to ANF.
module Fireflyc.Anf.Translate
  ( -- * Monad
    MonadTranslate
  , Environment (..)

    -- * Expressions
  , translateExpression
  ) where

import Control.Lens ((%~), at, makeLenses, view)
import Control.Monad (join)
import Control.Monad.Error.Class (MonadError)
import Control.Monad.Reader.Class (MonadReader)
import Data.HashMap.Strict (HashMap)

import qualified Control.Monad.Error.Class as Error
import qualified Control.Monad.Reader.Class as Reader
import qualified Data.HashMap.Strict as HashMap

import Fireflyc.Anf
import Fireflyc.Lc
import Fireflyc.Name

import qualified Fireflyc.Anf.Generate as AnfGen

--------------------------------------------------------------------------------
-- Monad

type MonadTranslate m =
  ( AnfGen.MonadGenerate m
  , MonadReader Environment m
  , MonadError Local m )

data Environment =
  Environment { _environmentLocals :: HashMap Local AnfLocal }

$(makeLenses ''Environment)

--------------------------------------------------------------------------------
-- Expressions

translateExpression :: MonadTranslate m => LcExpression 0 -> m AnfValue

translateExpression (LcVariable name) = do
  valueMaybe <- view (environmentLocals . at name)
  value <- maybe (Error.throwError name) pure valueMaybe
  AnfGen.value $ AnfVariable value

translateExpression (LcLambda parameters body) = do
  -- Generate an ANF local for each parameter.
  parameters' <- sequence [ (,) p <$> AnfGen.freshLocal | p <- parameters ]
  let parameters'' = HashMap.fromList parameters'

  -- Bring the parameters into scope and recurse into the body.
  (bindings, result) <-
    Reader.local (environmentLocals %~ HashMap.union parameters'') $
      AnfGen.rerunGenerateT (translateExpression body)
  let body' = AnfBlock { anfBindings = bindings, anfResult = result }

  -- Construct the ANF lambda.
  AnfGen.value $ AnfLambda (fmap snd parameters') body'

translateExpression (LcCall callee arguments) =
  AnfGen.call callee =<< traverse translateExpression arguments

translateExpression (LcApply applyee arguments) =
  join $ AnfGen.apply <$> translateExpression applyee
                      <*> traverse translateExpression arguments
