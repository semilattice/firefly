-- | PHP code generation.
module Fireflyc.Codegen.Php
  ( -- * Names
    codegenGlobalFunction

    -- * Expressions
  , codegenAnfBlock
  , codegenAnfExpression
  , codegenAnfValue

    -- * Intrinsics
  , codegenIntrinsic

    -- * Variables
  , codegenAnfLocal
  ) where

import Data.Bool (bool)
import Data.Foldable (fold)
import Data.HashSet (HashSet)
import Data.Int (Int64)
import Data.List (genericLength, intersperse)
import Data.Semigroup ((<>))
import Data.Text.Lazy.Builder (Builder)

import qualified Data.HashSet as HashSet
import qualified Data.Text.Lazy.Builder as Builder
import qualified Data.Text.Lazy.Builder.Int as Builder

import Fireflyc.Anf
import Fireflyc.Intrinsic
import Fireflyc.Name

--------------------------------------------------------------------------------
-- Names

-- | Generate the PHP name for a global function.
codegenGlobalFunction :: Global -> Word -> Builder
codegenGlobalFunction (Global (Namespace namespace) name) arity =
  "ff_function" <> namespace' <> "_" <> name' <> "_" <> arity'
  where namespace' = fold (fmap ("_" <>) (fmap Builder.fromText namespace))
        name' = Builder.fromText name
        arity' = Builder.decimal arity

--------------------------------------------------------------------------------
-- Expressions

-- | Generate a sequence of PHP statements for an ANF block. The given function
-- decides what to do with the result which is a PHP expression.
codegenAnfBlock :: AnfBlock -> (Builder -> Builder) -> Builder
codegenAnfBlock block withResult =
  foldMap (uncurry codegenAnfStatement) (anfBindings block) <>
  withResult (codegenAnfValue (anfResult block))
  where
  codegenAnfStatement :: AnfLocal -> AnfExpression -> Builder
  codegenAnfStatement local expression =
    codegenAnfExpression expression $ \result ->
      codegenAnfLocal local <> " = " <> result <> ";\n"

-- | Generate a sequence of PHP statements for an ANF expression. The given
-- function decides what to do with the result which is a PHP expression.
codegenAnfExpression :: AnfExpression -> (Builder -> Builder) -> Builder

codegenAnfExpression (AnfValue value) withResult =
  withResult $ codegenAnfValue value

codegenAnfExpression (AnfCall callee arguments) withResult =
  withResult $
    codegenGlobalFunction callee (genericLength arguments) <>
    codegenArguments (fmap codegenAnfValue arguments)

codegenAnfExpression (AnfApply applyee arguments) withResult =
  withResult $
    codegenAnfValue applyee <>
    codegenArguments (fmap codegenAnfValue arguments)

codegenAnfExpression (AnfIntrinsic intrinsic) withResult =
  codegenIntrinsic intrinsic withResult

-- | Generate a PHP expression for an ANF value.
codegenAnfValue :: AnfValue -> Builder

codegenAnfValue (AnfVariable local) =
  codegenAnfLocal local

codegenAnfValue lambda@(AnfLambda parameters body) =
  "(function" <> codegenParameters parameters <>
  codegenUses (anfValueFree lambda) <>
  " {\n" <> codegenAnfBlock body withResult <> "})"
  where
  withResult :: Builder -> Builder
  withResult result = "return " <> result <> ";\n"

codegenAnfValue (AnfBool value) =
  bool "FALSE" "TRUE" value

--------------------------------------------------------------------------------
-- Intrinsics

codegenIntrinsic :: Intrinsic AnfValue -> (Builder -> Builder) -> Builder
codegenIntrinsic = codegenIntrinsic' . fmap codegenAnfValue

codegenIntrinsic' :: Intrinsic Builder -> (Builder -> Builder) -> Builder

codegenIntrinsic' (NotBool a) withResult =
  withResult $ "!" <> a

codegenIntrinsic' (AndBools a b) withResult =
  withResult $ a <> " && " <> b

codegenIntrinsic' (OrBools a b) withResult =
  withResult $ a <> " || " <> b

codegenIntrinsic' (XorBools a b) withResult =
  withResult $ a <> " !== " <> b

codegenIntrinsic' (AddIntegers size a b) withResult =
  withResult $ a <> " + " <> b <> " & " <> mask
  where mask = Builder.decimal @Int64 (2 ^ integerSizeBits @Int size - 1)

codegenIntrinsic' (NotInteger size a) withResult =
  withResult $ "~" <> a <> " & " <> mask
  where mask = Builder.decimal @Int64 (2 ^ integerSizeBits @Int size - 1)

codegenIntrinsic' (AndIntegers _size a b) withResult =
  withResult $ a <> " & " <> b

codegenIntrinsic' (OrIntegers _size a b) withResult =
  withResult $ a <> " | " <> b

codegenIntrinsic' (XorIntegers _size a b) withResult =
  withResult $ a <> " ^ " <> b

codegenIntrinsic' (Panic message) withResult =
  "error_log(" <> message <> ");\n" <>
  "exit(1);\n" <>
  withResult "NULL"

--------------------------------------------------------------------------------
-- Variables

codegenAnfLocal :: AnfLocal -> Builder
codegenAnfLocal (AnfLocal n) =
  "$ff_local_" <> Builder.decimal n

--------------------------------------------------------------------------------
-- Auxiliary

-- | Generate a PHP argument list given some arguments. Includes the
-- parentheses enclosing the argument list.
codegenArguments :: [Builder] -> Builder
codegenArguments arguments = "(" <> fold (intersperse ", " arguments) <> ")"

-- | Generate a PHP parameter list given some parameters. Includes the
-- parentheses enclosing the parameter list.
codegenParameters :: [AnfLocal] -> Builder
codegenParameters parameters = "(" <> fold (intersperse ", " parameters') <> ")"
  where parameters' = fmap codegenAnfLocal parameters

-- | Generate a PHP use list for lambda expressions given some captured
-- variables. Includes the use keyword and the parentheses enclosing the use
-- list. Since use lists cannot be empty in PHP, if the given list is empty,
-- the empty string is returned.
codegenUses :: HashSet AnfLocal -> Builder
codegenUses uses
  | HashSet.null uses = ""
  | otherwise = " use(" <> fold (intersperse ", " uses') <> ")"
      where uses' = fmap codegenAnfLocal (HashSet.toList uses)
