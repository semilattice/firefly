-- | Names.
module Fireflyc.Name
  ( Namespace (..)
  , Global (..)
  , Local (..)
  ) where

import Data.Hashable (Hashable)
import Data.Text (Text)

newtype Namespace =
  Namespace [Text]
  deriving stock (Eq, Show)

data Global =
  Global Namespace Text
  deriving stock (Eq, Show)

newtype Local =
  Local Text
  deriving stock (Eq, Show)
  deriving newtype (Hashable)
