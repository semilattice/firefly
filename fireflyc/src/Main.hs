module Main
  ( main
  ) where

import Control.Monad ((<=<))
import Control.Monad.Trans.Except (runExceptT)
import Control.Monad.Trans.Reader (runReaderT)
import Data.Semigroup ((<>))

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Text.Lazy.Builder as Builder
import qualified Data.Text.Lazy.IO as Text.Lazy

import Fireflyc.Anf
import Fireflyc.Lc
import Fireflyc.Name

import Fireflyc.Anf.Generate (runGenerateT)
import Fireflyc.Anf.Translate (Environment (..), translateExpression)
import Fireflyc.Codegen.Php (codegenAnfBlock)

main :: IO ()
main = do
  block' <- block
  let php = codegenAnfBlock block' $ \result -> "return " <> result <> ";\n"
  let php' = Builder.toLazyText php
  Text.Lazy.putStr php'

block :: IO AnfBlock
block =
  let
    identity = LcLambda [Local "x"] (LcVariable (Local "x"))
    wrapped  = LcLambda [Local "x"] (LcApply identity [LcVariable (Local "x")])
    list     = LcCall (Global (Namespace []) "nil") []
    mapped   = LcCall (Global (Namespace []) "map") [list, wrapped]
  in
    run (translateExpression mapped)
  where
  run action =
    fmap (uncurry AnfBlock) $
      runGenerateT $
        either (fail . show) pure <=< runExceptT $
          runReaderT action (Environment HashMap.empty)
