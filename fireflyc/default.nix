{ mkDerivation, stdenv
, base, dlist, hashable, lens, mtl, text, transformers, unordered-containers }:
mkDerivation {
  pname = "fireflyc";
  version = "0.0.0.0";
  src = ./.;
  isExecutable = true;
  executableHaskellDepends = [
    base
    dlist
    hashable
    lens
    mtl
    text
    transformers
    unordered-containers
  ];
  license = null;
}
