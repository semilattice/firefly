{ pkgs ? import ./nix/nixpkgs.nix }:
let
  firefly = import ./. { pkgs = pkgs; };
in
  {
    fireflyc = firefly.fireflyc.env.overrideAttrs (p: {
      nativeBuildInputs =
        p.nativeBuildInputs ++
        [ pkgs.haskellPackages.ghcid pkgs.cabal-install ];
    });
  }
